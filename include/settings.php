<?php

class ComingSoon{

    function __construct() {

    }

    function addsubmenu(){
       ?>
        <h2 class="text-center">Coming Soon Template Settings</h2>
        <div class="panel with-nav-tabs panel-info">
            <div class="panel-heading">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1info" data-toggle="tab">Settings</a></li>
                    <li><a href="#tab2info" data-toggle="tab">Style</a></li>
                    <li><a href="#tab4info" data-toggle="tab">Subscribe</a></li>
                    <li><a href="#tab5info" data-toggle="tab">Custom Css</a></li>
                    <li><a href="#tab6info" data-toggle="tab">Preview</a></li>
                </ul>
            </div>
            <div class="panel-body">
                <?php
                if (!empty($_POST) && $_POST['updated'] === 'true') {
                    $this->handle_form();
                } ?>

                <form  method="post">
                    <input type="hidden" name="updated" value="true"/>
                    <?php wp_nonce_field('ps_cs_post_setting_form_update', 'ps_cs_related_post_setting_form'); ?>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab1info">
                        <?php
                       require_once('ps-cs-settings-tab.php');
                        ?>
                    </div>
                    <div class="tab-pane fade" id="tab2info">
                        <?php
                        require_once ('ps-cs-style-tabs.php');
                        ?>
                    </div>
                    <div class="tab-pane fade" id="tab4info">
                        <?php
                        require_once('ps-social-links.php');
                        ?>
                    </div>
                    <div class="tab-pane fade" id="tab5info">
                        <?php
                        require_once ('ps-custom-css.php');
                        ?>
                    </div>
                    <div class="tab-pane fade" id="tab6info">
                        <?php
                        require_once ('ps-cs-preview.php');
                        ?>
                    </div>
                    <p class="submit">
                        <input type="submit" name="submit" id="submit" class="button button-primary" value="Save Settings">
                    </p>
                </div>
                </form>
            </div>
        </div>
<?php
    }
    function handle_form(){
        if (!isset($_POST['ps_cs_related_post_setting_form']) || !wp_verify_nonce($_POST['ps_cs_related_post_setting_form'], 'ps_cs_post_setting_form_update')) { ?>
            <div class="error">
                <p>Sorry, your nonce was not correct. Please try again.</p>
            </div> <?php
            exit;
        } else {
            $ps_cs_facebook=$_POST['ps_cs_facebook'];
            $ps_cs_vimeo=$_POST['ps_cs_vimeo'];
            $ps_cs_tumblr=$_POST['ps_cs_tumblr'];
            $ps_cs_wordpress=$_POST['ps_cs_wordpress'];
            $ps_cs_twitter=$_POST['ps_cs_twitter'];

            $ps_cs_custom_css=$_POST['ps_cs_custom_css_area'];

            $ps_cs_themes_options_font=$_POST['ps_cs_themes_options_font'];
            $ps_cs_themes_options_type=$_POST['ps_cs_themes_options_type'];
            $ps_cs_logo=$_POST['ps_cs_logo'];
            $ps_cs_logo_file=$_POST['ps_cs_logo_file'];
            $ps_cs_background=$_POST['ps_cs_background'];

            $ps_cs_status=$_POST['ps_cs_status'];
            $ps_cs_launch_date=$_POST['ps_datetimepicker'];
            $ps_cs_auto_launch=$_POST['ps_cs_automatic_launch'];
            $ps_cs_display_site_tagline=$_POST['ps_cs_display_site_tagline'];
            $ps_cs_introduction_copy=$_POST['ps_cs_introduction_copy'];
            $ps_cs_intro_video=$_POST['ps_cs_intro_video'];
            $ps_cs_newsletter_embed_code=$_POST['ps_cs_news'];
            $ps_cs_obox_logo=$_POST['ps_cs_obox_logo'];
            $ps_cs_copyright_text=$_POST['ps_cs_copyright_text'];
            $ps_cs_apollo_theme_options=$_POST['ps_cs_apollo_theme_options'];


            $ps_cs_themes_options_theme=$_POST['emotion'];

            global $status;
            $status= $ps_cs_status;

            if ($_POST['ps_cs_related_post_setting_form']) {

                update_option('ps_cs_facebook', $ps_cs_facebook);
                update_option('ps_cs_vimeo', $ps_cs_vimeo);
                update_option('ps_cs_tumblr',$ps_cs_tumblr);
                update_option('ps_cs_wordpress', $ps_cs_wordpress);
                update_option('ps_cs_twitter',$ps_cs_twitter);


                update_option('ps_cs_custom_css_area',$ps_cs_custom_css);


                update_option('emotion',$ps_cs_themes_options_theme);
                update_option('ps_cs_themes_options_font',$ps_cs_themes_options_font);
                update_option('ps_cs_themes_options_type', $ps_cs_themes_options_type);
                update_option('ps_cs_logo',$ps_cs_logo);
                update_option('ps_cs_logo_file',$ps_cs_logo_file);
                update_option('ps_cs_background',$ps_cs_background);

                update_option('ps_cs_status', $ps_cs_status);
                update_option('ps_datetimepicker', $ps_cs_launch_date);
                update_option('ps_cs_automatic_launch',$ps_cs_auto_launch);
                update_option('ps_cs_display_site_tagline',$ps_cs_display_site_tagline);
                update_option('ps_cs_introduction_copy',$ps_cs_introduction_copy);
                update_option('ps_cs_intro_video',$ps_cs_intro_video);
                update_option('ps_cs_news',$ps_cs_newsletter_embed_code);
                update_option('ps_cs_obox_logo',$ps_cs_obox_logo);
                update_option('ps_cs_copyright_text',$ps_cs_copyright_text);
                update_option('ps_cs_apollo_theme_options',$ps_cs_apollo_theme_options);

                ?>
                <div class="updated">
                    <p>Your fields were saved!</p>
                </div> <?php
            } else { ?>
                <div class="error">
                    <p>Opps Something Wrong here</p>
                </div> <?php
            }
        }
    }
}

?>