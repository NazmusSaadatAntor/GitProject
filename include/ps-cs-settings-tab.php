


<h1>Basic Setings</h1>
<div class="ps-button-up">
    <p class="top">
        Activate your landing page, set the launch date and add your copy. Get started here
    </p><hr/>

</div>


<table class="form-table">
    <tr>
        <th scope="row">Status</th>
        <td>OFF
            <label class="switch">
                <input name="ps_cs_status" type="checkbox">
                <div class="slider round"></div>
            </label>
            ON
        </td>
    </tr>

    <tr>


    </tr>
    <th scope="row">Launch Date</th>
    <td>
        <input type="date"  class="form-control" id="ps_datetimepicker" name='ps_datetimepicker' />
        <div><span class="description">When does it launch ?</span></div>
    </td>
    <tr>
        <th scope="row">Automatic Launch</th>
        <td>
            <input type="checkbox" name="ps_cs_automatic_launch" checked/>
            <span>Check this ON to automatically disable the plugin after the launch date</span>
        </td>
    </tr>
    <tr>
        <th scope="row">Display Site Tagline</th>
        <td>
            <div class="compound-setting">
                <input type="checkbox" name="ps_cs_display_site_tagline" checked/>
            </div>
        </td>
    </tr>

    <tr>
        <th scope="row">Introduction Copy</th>
        <td>
            <textarea rows="5" cols="60" name="ps_cs_introduction_copy"></textarea>
        </td>
    </tr>
    <tr>
        <th scope="row">Intro Video</th>
        <td>
            <input class="long-input" name="ps_cs_intro_video" type="text" value=""/>
            <div><span class="description">If you have a video, enter it's URL here.</span></div>
        </td>
    </tr>
    <tr>
        <th scope="row">Newsletter Embed Code</th>
        <td>
            <!--            <input rows="5" cols="60" name="ps_cs_newsletter_code">-->
            <!--            <div><span class="description">Newsletter signup form embed code.</span>-->
            <!--            </div>-->
            <textarea type="text" name="ps_cs_news" ></textarea>
        </td>
    </tr>



    <tr>
        <th scope="row">Display Obox Logo</th>
        <td>
            <input type="checkbox" name="ps_cs_obox_logo" checked/>
            <span>Display the Obox logo, creators of The Launchpad, in your footer.</span>
        </td>
    </tr>
    <tr>
        <th scope="row">Footer Copyright Text</th>
        <td>
            <div class="compound-setting">
                <input class="long-input" name="ps_cs_copyright_text" type="text" value="" placeholder="Copyright admin.dev 2017."/>
            </div>
        </td>
    </tr>


    <tr>
        <th scope="row">Minimum User Rights</th>
        <td>
            <select id="theme" name="ps_cs_apollo_theme_options">

                <option value="grunge" selected="selected">Grunge</option>

                <option value="minimal">Minimal</option>

                <option value="slick-gloss">Slick Gloss</option>
            </select>
            <span>Select which users are able to access the front end site.</span>
        </td>
    </tr>
</table>
