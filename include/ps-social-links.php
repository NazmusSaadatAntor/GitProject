

<h1>Social Link Options</h1>
<div class="ps-button-up">
    <p class="top">
        Enter in your social network URLs. Leave blank to hide the buttons.
    </p>
    <hr/>
</div>

<div class="ps-social-form">
    <form>
        <div class="form-group row">
            <label  class="col-sm-2 col-form-label">Facebook</label>
            <div class="col-sm-6">
                <input type="text" class="form-control"  name="ps_cs_facebook" >
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 col-form-label">Vimeo</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="ps_cs_vimeo">
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 col-form-label">Tumblr</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="ps_cs_tumblr" >
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 col-form-label">Wordpress</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="ps_cs_wordpress">
            </div>
        </div>
        <div class="form-group row">
            <label  class="col-sm-2 col-form-label">Twitter</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="ps_cs_twitter" >
            </div>
        </div>
    </form>
</div>

