

            <h1 class="theme-display">Theme & Display Options</h1>


            <p>Setup the look & feel of your launch page.<a href="#"><span class="get-some">Get some cool backgrounds here</span></a>
            </p><hr/>
            <table class="form-table"><tbody><tr><th scope="row">Theme</th><td>
                        <input
                                type="radio" name="emotion" value="style1"
                                id="sad" class="input-hidden" />
                        <label for="sad">
                            <img
                                    src="//placekitten.com/150/150"
                                    alt="I'm sad" />
                        </label>

                        <input
                                type="radio" name="emotion" value="style2"
                                id="happy" class="input-hidden" />
                        <label for="happy">
                            <img
                                    src="//placekitten.com/151/151"
                                    alt="I'm happy" />
                        </label>
                    </td></tr><tr><th scope="row">Font</th><td>
                        <select id="font" name="ps_cs_themes_options_font">

                            <option value="" selected="selected">-- Theme Default --</option>

                            <option value="sans-serif-style">Sans Serif</option>

                            <option value="serif-sans-style">Serif Sans Serif</option>

                            <option value="serif-style">Serif</option>
                        </select>
                    </td></tr><tr><th scope="row">Logo</th><td>
                        <input id="ps_cs_logo" data-input-key="logo" name="ps_cs_logo" type="checkbox" onclick="if(this.checked){myFunction()}else if(this.checked==false){myFunction1()}">
                        <label class="clear" for="clear-logo">
                            Enable logo			</label>
                        <input style="visibility: hidden" type="file" name="pic" id="ps_cs_upload_logo" accept="image/*">

                    </td></tr><tr><th scope="row">Background</th><td>
                        <input id="clear-background" data-input-key="background" name="ps_cs_background" type="checkbox">
                        <label class="clear" for="clear-background">
                            Enable background			</label>
                    </td></tr></tbody>
            </table>
            <script>
                function myFunction() {
                        document.getElementById("ps_cs_upload_logo").style.visibility = "visible";
                }
                function myFunction1() {
                    document.getElementById("ps_cs_upload_logo").style.visibility = "hidden";
                }
            </script>