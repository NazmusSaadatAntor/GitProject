
<!--Video Section--> 

<a id="video" class="player" data-property="{videoURL:'https://www.youtube.com/watch?v=TVzZjulH2MM',containment:'.bg-container-youtube', showControls:false, autoPlay:true, loop:true,  startAt:0, opacity:1, addRaster:false, quality:'large'}"></a>
<div class="bg-container-youtube"></div>
<div class="overlay-bg"></div>

<!--Video Section--> 

<!-- main -->

<main role="video-container"> 
  
  <!-- container -->
  
  <div class="container"> 
    
    <!-- tab-content -->
    
    <div class="tab-content text-center"> 
      
      <!-- Countdown -->
      
      <section id="home" class="tab-pane fade in active">
        <article role="countdown" class="countdown-pan">
          <div id="countdown" class="text-center"></div>
          <p>we are working hard for better expirience</p>
        </article>
      </section>
      
      <!-- Countdown --> 
      
      <!-- Subscribe -->
      
      <section id="menu2" class="tab-pane fade">
        <article role="subscribe" class="subscribe-pan">
          <header class="page-title">
            <h2>Subscribe to Us</h2>
          </header>
          <div class="ntify_form">
            <form method="post" action="http://designstub.com/demos/pixicon/pixicon/php/subscribe.php" name="subscribeform" id="subscribeform">
              <input name="email" type="email" id="subemail" placeholder="Enter Your Email...">
              <label>
                <input name="" type="submit" class="button-icon">
                <i class="fa fa-paper-plane" aria-hidden="true"></i> </label>
            </form>
            
            <!-- subscribe message -->
            
            <div id="mesaj"></div>
            
            <!-- subscribe message --> 
            
          </div>
          <p>Please enter your email below and we'll let you know once<br/>
            we're up and running.</p>
        </article>
      </section>
      
      <!-- Subscribe --> 
      
      <!-- Contact -->
      
      <section id="menu3" class="tab-pane fade">
        <article role="contact" class="contact-pan">
          <header class="page-title">
            <h2>Stay in touch with us</h2>
          </header>
          <h3><a href="mailto:Contact@pixicon.com"> info@pranon.com</a></h3>
          <ul>
            <li><i class="fa fa-map-marker" aria-hidden="true"></i> House No: 1364, Road No: 12, Avenue: 02, Mirpur DOHS, Dhaka, Bangladesh.</li>
            <li><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:1-212-290-4700">+8800001112440</a></li>
          </ul>
        </article>
      </section>
    </div>
    
    <!-- tab-content --> 
    
  </div>
  
  <!-- container --> 
  
</main>

<!-- main --> 

<!--             

                    </div>

                </section>--> 

<!--Video Section--> 

<!-- header -->

<header role="header">
  <hgroup> 
    
    <!-- logog -->
    
    <h1> <a href="#" title="Pixicon">Pranon</a> </h1>
    
    <!-- logog --> 
    
    <!-- nav -->
    
    <nav role="nav" id="header-nav" class="nav navy">
      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home" title="Countdown">Countdown</a></li>
        <li><a data-toggle="tab" href="#menu2" title="Subscribe">Subscribe</a></li>
        <li><a data-toggle="tab" href="#menu3" title="Contact">Contact</a></li>
      </ul>
      <div role="socil-icons" class="mobile-social">
        <li><a href="#" target="_blank" title="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        <li><a href="#" target="_blank" title="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#" target="_blank" title="google-plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
        <li><a href="#" target="_blank" title="pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
      </div>
    </nav>
    
    <!-- nav --> 
    
    <!-- Socil Icon -->
    
    <ul role="socil-icons" class="desk-social">
      <li><a href="#" target="_blank" title="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
      <li><a href="#" target="_blank" title="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
      <li><a href="#" target="_blank" title="google-plus"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
      <li><a href="#" target="_blank" title="pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
    </ul>
    
    <!-- Socil Icon --> 
    
  </hgroup>
  <footer class="desk">
    <p>Copyright 2016 Pranon Group | All Rights Reserved <i class="fa fa-heart" aria-hidden="true"></i> by Pranon</p>
  </footer>
</header>

<!-- header -->

<footer class="mobile">
  <p>
Copyright 2016 Pranon Group | All Rights Reserved<i class="fa fa-heart" aria-hidden="true"></i> by Pranon</p>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 

<script src="style2/js/jquery.min.js" type="text/javascript"></script>

<!-- custom --> 

<script src="style2/js/custom.js" type="text/javascript"></script>
<script src="style2/js/nav-custom.js" type="text/javascript"></script>

<!-- Include all compiled plugins (below), or include individual files as needed --> 

<script src="style2/js/bootstrap.min.js" type="text/javascript"></script>

<!-- jquery.countdown --> 

<script src="style2/js/countdown-js.js" type="text/javascript"></script>

<!-- Video --> 

<script src="style2/js/jquery.mb.YTPlayer.js" type="text/javascript"></script>
<script src="style2/js/video.js" type="text/javascript"></script>
<script src="style2/js/html5shiv.js" type="text/javascript"></script>

<?php wp_head(); ?>
