<?php
$date=get_option( 'ps_datetimepicker' );
list($year, $day,$monthNum ) = explode("-",$date);
$dateObj = DateTime::createFromFormat('!m', $monthNum);
$month = $dateObj->format('F');
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta name="author" content="webthemez">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pranon Studio Coming Soon template | webthemez</title>

</head>

<body class="banner-area">
<!--header section -->
<section class="banner" role="banner">
    <!-- overlay -->
    <div class="banner-area-gradient"></div>
    <!-- overlay -->
    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="banner-text text-center"> <a class="logo" href="#">Prano<b>n</b></a>

                        <div class="type-wrap">
                            <div id="typed-strings">
                                <p>We will launch some <em> Plugins.</em></p>
                                <span>Stay Tuned For Something <br/><strong>Amazing</strong></span>
                            </div>
                            <span id="typed" style="white-space:pre;"></span>
                        </div>
                        <!--Countdown -->
                        <input type="hidden" id="ps_month" value="<?php echo $month; ?>">
                        <input type="hidden" id="ps_year" value="<?php echo $year; ?>">
                        <input type="hidden" id="ps_date" value="<?php echo $day; ?>">
                        <div id="countdown"></div>
                        </p>
                    </div>
                </div>
            </div>

            <!--subscribe section -->

        </div>
    </div>
    <!-- Footer section -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <ul class="footer-share">
                        <li><a href="<?php echo get_option( 'ps_cs_facebook' )?>"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="<?php echo get_option( 'ps_cs_vimeo' )?>"><i class="fa fa-vimeo"></i></a></li>
                        <li><a href="<?php echo get_option( 'ps_cs_wordpress' )?>"><i class="fa fa-wordpress"></i></a></li>
                        <li><a href="<?php echo get_option( 'ps_cs_twitter' )?>"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="<?php echo get_option( 'ps_cs_tumblr')?>"><i class="fa fa-tumblr"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer section -->
</section>
<!--header section -->

<!-- JS files-->
<?php
wp_head();
?>
</body>

</html>