// $=jQuery;
$nc=jQuery.noConflict();

jQuery(document).ready(function() {	
	
    /*
        Background slideshow
    */
	$nc('.banner-area').backstretch([
	                     "/wp-content/plugins/PranonStudioCommingSoon/themes/style1/images/backgrounds/1.jpg"
	                   , "/wp-content/plugins/PranonStudioCommingSoon/themes/style1/images/backgrounds/2.jpg"
	                   , "/wp-content/plugins/PranonStudioCommingSoon/themes/style1/images/backgrounds/3.jpg"
	                  ], {duration: 3000, fade: 750});
	

	$nc("#typed").typed({
		// strings: ["Typed.js is a <strong>jQuery</strong> plugin.", "It <em>types</em> out sentences.", "And then deletes them.", "Try it out!"],
		stringsElement: $nc('#typed-strings'),
		typeSpeed: 50,
		backDelay: 1000,
		loop: true,
		contentType: 'html', // or text
		// defaults to false for infinite loop
		loopCount: false,
		callback: function(){ foo(); },
		resetCallback: function() { newTyped(); }
	});

	$nc(".reset").click(function(){
		$nc("#typed").typed('reset');
	});
 
 
    function newTyped(){ /* A new typed object */ }

    function foo(){ console.log("Callback"); }

	
});

// cowntdown function. Set the date below (December 1, 2016 00:00:00):
var austDay = new Date($nc('#ps_month').val()+"  "+$nc('#ps_date').val()+",  "+$nc('#ps_year').val()+" 00:00:00");
	$nc('#countdown').countdown({until: austDay, layout: '<div class="item"><p>{dn}</p> {dl}</div> <div class="item"><p>{hn}</p> {hl}</div> <div class="item"><p>{mn}</p> {ml}</div> <div class="item"><p>{sn}</p> {sl}</div>'});
	$nc('#year').text(austDay.getFullYear());
	
// smooth scrolling	
	$nc(function() {
  $nc('a[href*=#]:not([href=#])').click(function() {
	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

	  var target = $nc(this.hash);
	  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	  if (target.length) {
		$nc('html,body').animate({
		  scrollTop: target.offset().top
		}, 1000);
		return false;
	  }
	}
  });
});
