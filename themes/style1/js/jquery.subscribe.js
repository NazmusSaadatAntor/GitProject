$nc=jQuery.noConflict();
jQuery(document).ready(function(){
	$nc('#subscribeform').submit(function(){

		var action = $(this).attr('action');

		$nc("#mesaj").slideUp(750,function() {
		$nc('#mesaj').hide();

 		$nc('#subsubmit')
			.after('')
			.attr('disabled','disabled');

		$.post(action, {
			email: $nc('#subemail').val()
		},
			function(data){
				document.getElementById('mesaj').innerHTML = data;
				$nc('#mesaj').slideDown('slow');
				$nc('#subscribeform img.subscribe-loader').fadeOut('slow',function(){$(this).remove()});
				$nc('#subsubmit').removeAttr('disabled');
				if(data.match('success') != null) $('#subscribeform').slideUp('slow');

			}
		);

		});

		return false;

	});

});