<?php
/**
 * Plugin Name: Ps Coming Soon
 * Description: Let people know that your site is 'coming soon'. Visitors can submit their email addresses for future notification.
 * Plugin URI: pranonstudio.com
 * Author: Pranon Studio LLC
 * Author URI:pranonstudio.com
 * Version: 1.0
 * License: GPL2
 * Text Domain: ps-coming-soon
 */

/**
 * Copyright (c) . All rights reserved.
 *
 * Released under the GPL license
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * This is an add-on for WordPress
 * http://wordpress.org/
 *
 * **********************************************************************
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * **********************************************************************
 */

// don't call the file directly
if ( !defined( 'ABSPATH' ) ) exit;

// check for required php version and deactivate the plugin if php version is less.
if ( version_compare( PHP_VERSION, '5.4', '<' )) {
    add_action( 'admin_notices', 'ps_notice' );
    function ps_notice() { ?>
        <div class="error notice is-dismissible"> <p>
                <?php
                echo 'This requires minimum PHP 5.4 to function properly. Please upgrade PHP version. The Plugin has been auto-deactivated.. You have PHP version '.PHP_VERSION;
                ?>
            </p></div>
        <?php
        if ( isset( $_GET['activate'] ) ) {
            unset( $_GET['activate'] );
        }
    }

    // deactivate the plugin because required php version is less.
    add_action( 'admin_init', 'ps_deactivate_self' );
    function ps_deactivate_self() {
        deactivate_plugins( plugin_basename( __FILE__ ) );
    }
    return;
}

//----------------------------------------------------------------------
// Core constant defination
//----------------------------------------------------------------------
if (!defined('PS_COMING_SOON_PLUGIN_VERSION')) define( 'PS_COMING_SOON_PLUGIN_VERSION', '1.0.0' );
if (!defined('PS_COMING_SOON_PLUGIN_BASENAME')) define( 'PS_COMING_SOON_PLUGIN_BASENAME', plugin_basename(__FILE__) );
if (!defined('PS_COMING_SOON_MINIMUM_WP_VERSION')) define( 'PS_COMMING_SOON_MINIMUM_WP_VERSION', '3.5' );
if (!defined('PS_COMING_SOON_PLUGIN_PLUGIN_DIR')) define( 'PS_COMING_SOON_PLUGIN_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
if (!defined('PS_COMING_SOON_PLUGIN_URI')) define( 'PS_COMING_SOON_PLUGIN_URI', plugins_url('', __FILE__) );
if (!defined('PS_COMING_SOON_PLUGIN_TEXTDOMAIN')) define( 'PS_COMING_SOONPLUGIN_TEXTDOMAIN', 'ps-coming-soon' );




//.........................
//requireFiles
//.............................
require_once("include/settings.php");
require_once("themeConfiguration.php");
//...........................



//------------------------------------------
//add admin menu
//---------------------------------------

//include admin style
function ps_coming_soon_admin_enqueue_styles() {
    //Register Style
    wp_register_style('ps-coming-soon-admin-style', PS_COMING_SOON_PLUGIN_URI. '/admin/css/styles.css',true, PS_COMING_SOON_PLUGIN_VERSION);
    wp_register_style('ps-coming-soon-admin-bootstrap-style', PS_COMING_SOON_PLUGIN_URI. '/admin/css/bootstrap.min.css',true, PS_COMING_SOON_PLUGIN_VERSION);
    //register Scripts
    wp_register_script('ps-coming-soon-admin-script', PS_COMING_SOON_PLUGIN_URI. '/admin/js/bootstrap.min.js', array('jquery'), PS_COMING_SOON_PLUGIN_VERSION, true);
    wp_register_script('ps-coming-soon-admin-adminjs-script', PS_COMING_SOON_PLUGIN_URI. '/admin/js/adminjs.js', PS_COMING_SOON_PLUGIN_VERSION, true);
    wp_register_script('ps-coming-soon-admin-jquery-ui-script', PS_COMING_SOON_PLUGIN_URI. '/admin/js/jquery-ui.js', array('jquery'), PS_COMING_SOON_PLUGIN_VERSION, true);

        //Enqueing styles and script
        wp_enqueue_style('ps-coming-soon-admin-style');
        wp_enqueue_style('ps-coming-soon-admin-bootstrap-style');
        wp_enqueue_script('ps-coming-soon-admin-script');
        wp_enqueue_script('ps-coming-soon-admin-adminjs-script');
        wp_enqueue_script('ps-coming-soon-admin-jquery-ui-script');
}

add_action( 'admin_enqueue_scripts', 'ps_coming_soon_admin_enqueue_styles' );

add_action('admin_menu', 'ps_cs_add_admin_menu');

function ps_cs_add_admin_menu(){
    add_menu_page("Coming Soon Settings","Coming Soon",4,"cs-settings",'ps_cs_menu_settings');
}

function ps_cs_menu_settings(){
    $submenu=new ComingSoon();
    $submenu->addsubmenu();
}


function enquestyle(){

    //Register style1

    wp_register_style('ps-coming-soon-style1-main', PS_COMING_SOON_PLUGIN_URI. '/themes/style1/css/main.css',true,  PS_COMING_SOON_PLUGIN_VERSION);
    wp_register_style('ps-coming-soon-style1-bootstrap', PS_COMING_SOON_PLUGIN_URI. '/themes/style1/css/bootstrap.min.css',true,  PS_COMING_SOON_PLUGIN_VERSION);
    wp_register_style('ps-coming-soon-style1-responsive', PS_COMING_SOON_PLUGIN_URI. '/themes/style1/css/responsive.css',true,  PS_COMING_SOON_PLUGIN_VERSION);
    wp_register_style('ps-coming-soon-style1-font', PS_COMING_SOON_PLUGIN_URI. '/themes/style1/css/font-awesome.min.css',true,  PS_COMING_SOON_PLUGIN_VERSION);

    //script
    wp_register_script('ps-coming-soon-style1-script', PS_COMING_SOON_PLUGIN_URI. '/themes/style1/js/bootstrap.min.js', array('jquery'), PS_COMING_SOON_PLUGIN_VERSION, true);
    wp_register_script('ps-coming-soon-style1-backstrach', PS_COMING_SOON_PLUGIN_URI. '/themes/style1/js/jquery.backstretch.min.js', PS_COMING_SOON_PLUGIN_VERSION, true);
    wp_register_script('ps-coming-soon-style1-countdown', PS_COMING_SOON_PLUGIN_URI. '/themes/style1/js/jquery.countdown.js', PS_COMING_SOON_PLUGIN_VERSION, true);
    wp_register_script('ps-coming-soon-subscribe', PS_COMING_SOON_PLUGIN_URI. '/themes/style1/js/jquery.subscribe.js', PS_COMING_SOON_PLUGIN_VERSION, true);
    wp_register_script('ps-coming-soon-style1-typemin', PS_COMING_SOON_PLUGIN_URI. '/themes/style1/js/typed.min.js', PS_COMING_SOON_PLUGIN_VERSION, true);
    wp_register_script('ps-coming-soon-style1-mainjs', PS_COMING_SOON_PLUGIN_URI. '/themes/style1/js/main.js',  PS_COMING_SOON_PLUGIN_VERSION, true);
    wp_register_script('ps-coming-soon-style1-jquery', PS_COMING_SOON_PLUGIN_URI. '/themes/style1/js/jquery-3.2.1.min.js', array('jquery'), PS_COMING_SOON_PLUGIN_VERSION, true);



    //style2
    wp_register_style('ps-coming-soon-style2-main', PS_COMING_SOON_PLUGIN_URI. '/themes/style2/css/style.css',true,  PS_COMING_SOON_PLUGIN_VERSION);
    wp_register_style('ps-coming-soon-style2-video', PS_COMING_SOON_PLUGIN_URI. '/themes/style2/css/video.css',true,  PS_COMING_SOON_PLUGIN_VERSION);


    //script
    wp_register_script('ps-coming-soon-style2-countdown', PS_COMING_SOON_PLUGIN_URI. '/themes/style2/js/countdown-js.js', array('jquery'), PS_COMING_SOON_PLUGIN_VERSION, true);
    wp_register_script('ps-coming-soon-style2-customjs', PS_COMING_SOON_PLUGIN_URI. '/themes/style2/js/custom.js', array('jquery'), PS_COMING_SOON_PLUGIN_VERSION, true);
    wp_register_script('ps-coming-soon-style2-html5shiv', PS_COMING_SOON_PLUGIN_URI. '/themes/style2/js/html5shiv.js', array('jquery'), PS_COMING_SOON_PLUGIN_VERSION, true);
    wp_register_script('ps-coming-soon-style2-jquery.mb.YTPlayer', PS_COMING_SOON_PLUGIN_URI. '/themes/style2/js/jquery.mb.YTPlayer.js', array('jquery'), PS_COMING_SOON_PLUGIN_VERSION, true);
    wp_register_script('ps-coming-soon-style2-nav-custom', PS_COMING_SOON_PLUGIN_URI. '/themes/style2/js/nav-custom.js', array('jquery'), PS_COMING_SOON_PLUGIN_VERSION, true);
    wp_register_script('ps-coming-soon-style2-video', PS_COMING_SOON_PLUGIN_URI. '/themes/style2/js/video.js', array('jquery'), PS_COMING_SOON_PLUGIN_VERSION, true);




    //Enqueue style1
    wp_enqueue_style('ps-coming-soon-style1-main');
    wp_enqueue_style('ps-coming-soon-style1-bootstrap');
    wp_enqueue_style('ps-coming-soon-style1-responsive');
    wp_enqueue_style('ps-coming-soon-style1-font');

    wp_enqueue_script('ps-coming-soon-style1-jquery');
    wp_enqueue_script('ps-coming-soon-style1-script');
    wp_enqueue_script('ps-coming-soon-style1-backstrach');
    wp_enqueue_script('ps-coming-soon-style1-countdown');
    wp_enqueue_script('ps-coming-soon-style1-mainjs');
    wp_enqueue_script('ps-coming-soon-subscribe');
    wp_enqueue_script('ps-coming-soon-style1-typemin');



    //Enqueue style2
    wp_enqueue_style('ps-coming-soon-style2-main');
    wp_enqueue_style('ps-coming-soon-style2-video');


    wp_enqueue_script('ps-coming-soon-style2-countdown');
    wp_enqueue_script('ps-coming-soon-style2-customjs');
    wp_enqueue_script('ps-coming-soon-style2-html5shiv');
    wp_enqueue_script('ps-coming-soon-style2-jquery.mb.YTPlayer');
    wp_enqueue_script('ps-coming-soon-style2-nav-custom');
    wp_enqueue_script('ps-coming-soon-style2-video');


}

add_action('wp_enqueue_scripts','enquestyle');

function ps_cs_my_page_redirect_template() {
    if (get_option( 'ps_cs_status' )=='on') {
        if (is_user_logged_in()) {
            if (!is_admin()){
               // $file1 = plugin_dir_path(__FILE__) . '/themes/style1.php';
                $file2 = plugin_dir_path(__FILE__) . '/themes/style2.php';
              //  include($file1);
                   include($file2);
                exit();
            }
        }
    }
}

add_action('template_redirect', 'ps_cs_my_page_redirect_template');





//--------------------------------------------------
// add user
//--------------------------------------------------


?>
